#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
  #include "Platform.h"
  #include "SoftwareSerial.h"
#ifndef CDC_ENABLED
  // Shield Jumper on SW
  SoftwareSerial port(12,13);
#else
  // Shield Jumper on HW (for Leonardo)
  #define port Serial1
#endif
#else // Arduino 0022 - use modified NewSoftSerial
  #include "WProgram.h"
  #include "NewSoftSerial.h"
  NewSoftSerial port(12,13);
#endif

#include "EasyVR.h"
EasyVR easyvr(port);
//Groups and Commands
enum Groups
{
  GROUP_0  = 0,
  GROUP_1  = 1,
};

enum Group0 
{
  G0_OK_CLOCK = 0,
};

enum Group1 
{
  G1_CHICAGO = 0,
  G1_BERLIN = 1,
  G1_LONDON = 2,
  G1_NEW_YORK = 3,
  G1_TOKYO = 4,
  G1_SAN_FRANCISCO = 5,
};

EasyVRBridge bridge;
int8_t group, idx;
// Signifies clock is changing time zones
boolean zone_changing = false;
// Motor Control Pins
int motorPin1 =  5;    // One motor wire connected to digital pin 5
int motorPin2 =  6;    // One motor wire connected to digital pin 6
int dutyCycle = 200;
int delayTime = 20;


void setup()
{
#ifndef CDC_ENABLED
  // bridge mode?
  if (bridge.check())
  {
    cli();
    bridge.loop(0, 1, 12, 13);
  }

  // run normally
  Serial.begin(9600);
  Serial.println("Bridge not started!");
#else
  // bridge mode?
  if (bridge.check())
  {
    port.begin(9600);
    bridge.loop(port);
  }
  Serial.println("Bridge connection aborted!");
#endif
  port.begin(9600);

  while (!easyvr.detect())
  {
    Serial.println("EasyVR not detected!");
    delay(1000);
  }

  easyvr.setPinOutput(EasyVR::IO1, LOW);
  Serial.println("EasyVR detected!");
  easyvr.setTimeout(5);
  easyvr.setLanguage(0);
  group = EasyVR::TRIGGER; //<-- start group (customize)
  // Set Motor Pins for clock
  pinMode(motorPin1, OUTPUT); 
  pinMode(motorPin2, OUTPUT); 

}

void action();

void loop()
{
  easyvr.setPinOutput(EasyVR::IO1, HIGH); // LED on (listening)
  Serial.print("Say a command in Group ");
  Serial.println(group);
  easyvr.recognizeCommand(group);

  do
  {
    if (!zone_changing)
    {
      clock_tick();
    }
    // can do some processing while waiting for a spoken command
  }

  while (!easyvr.hasFinished());
  easyvr.setPinOutput(EasyVR::IO1, LOW); // LED off
  idx = easyvr.getWord();

  if (idx >= 0)
  {

    // built-in trigger (ROBOT)

    // group = GROUP_X; <-- jump to another group X

    return;
  }

  idx = easyvr.getCommand();
  if (idx >= 0)
  {

    // print debug message
    uint8_t train = 0;
    char name[32];
    Serial.print("Command: ");
    Serial.print(idx);
    if (easyvr.dumpCommand(group, idx, name, train))
    {
      Serial.print(" = ");
      Serial.println(name);
    }

    else
      Serial.println();
    easyvr.playSound(0, EasyVR::VOL_FULL);
    // perform some action
    action();
  }
  else // errors or timeout
  {
    if (easyvr.isTimeout())
      Serial.println("Timed out, try again...");
    int16_t err = easyvr.getError();
    if (err >= 0)
    {
      Serial.print("Error ");
      Serial.println(err, HEX);
    }
  }
}


void action()
{
    switch (group)
    {
    case GROUP_0:
      switch (idx)
      {
      case G0_OK_CLOCK:
        // write your action code here
        group = GROUP_1;
        // group = GROUP_X; <-- or jump to another group X for composite commands
        break;
      }
      break;
    case GROUP_1:
      switch (idx)
      {
      case G1_CHICAGO:
        // write your action code here
        rotate(.5);
        group = GROUP_0;
        break;
      case G1_BERLIN:
        // write your action code here
        group = GROUP_0;
        break;
      case G1_LONDON:
        // write your action code here
        group = GROUP_0;
        break;
      case G1_NEW_YORK:
        // write your action code here
        group = GROUP_0;
        break;
      case G1_TOKYO:
        // write your action code here
        group = GROUP_0;
        break;
      case G1_SAN_FRANCISCO:
        // write your action code here
        group = GROUP_0;
        break;
      }
      break;
    }
}

// Input time in Hours +/-
void rotate(double time)
{
  // Signify that we are changing time zones to avoid regular clock motion
  zone_changing = true;
  // Convert Hours into Seconds (Cycles)
  time = time * 1800;
  double distance = abs(time);
  while(distance > 0)
  {
    distance--;
    analogWrite(motorPin1, dutyCycle); //rotates motor
    digitalWrite(motorPin2, LOW);    // set the Pin motorPin2 LOW
    delay(delayTime);
    analogWrite(motorPin2, dutyCycle); //rotates motor
    digitalWrite(motorPin1, LOW);    // set the Pin motorPin2 LOW
    delay(delayTime);  
  }
  // Resume regular clock motion now that we are no longer switching time zones
  zone_changing = false;
}

// Clock Tick Moves
void clock_tick()
{ 
    analogWrite(motorPin1, dutyCycle); //rotates motor
    digitalWrite(motorPin2, LOW);    // set the Pin motorPin2 LOW
    delay(1000);
    analogWrite(motorPin2, dutyCycle); //rotates motor
    digitalWrite(motorPin1, LOW);    // set the Pin motorPin2 LOW
    delay(1000);  
}


