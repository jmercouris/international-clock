//2-Way motor control

int motorPin1 =  5;    // One motor wire connected to digital pin 5
int motorPin2 =  6;    // One motor wire connected to digital pin 6
int dutyCycle = 200;
int delayTime = 100;

// The setup() method runs once, when the sketch starts

void setup()   {                
  // initialize the digital pins as an output:
  pinMode(motorPin1, OUTPUT); 
  pinMode(motorPin2, OUTPUT); 
  //rotate(1);
  //accelerate();
}

// the loop() method runs over and over again,
// as long as the Arduino has power
void loop()                     
{
  clock_tick();
}

// Input time in Hours +/-
void rotate(int time)
{
  // Convert Hours into Seconds (Cycles)
  time = time * 1800;
  int distance = abs(time);
  while(distance > 0)
  {
    distance--;
    analogWrite(motorPin1, dutyCycle); //rotates motor
    digitalWrite(motorPin2, LOW);    // set the Pin motorPin2 LOW
    delay(delayTime);
    analogWrite(motorPin2, dutyCycle); //rotates motor
    digitalWrite(motorPin1, LOW);    // set the Pin motorPin2 LOW
    delay(delayTime);  
  }
}

void clock_tick()
{ 
    analogWrite(motorPin1, dutyCycle); //rotates motor
    digitalWrite(motorPin2, LOW);    // set the Pin motorPin2 LOW
    delay(1000);
    analogWrite(motorPin2, dutyCycle); //rotates motor
    digitalWrite(motorPin1, LOW);    // set the Pin motorPin2 LOW
    delay(1000);  
}

// Peripheral does not behave as expected
// Jams gears while accelerating
void accelerate()
{
  while(delayTime > 20)
  {
    int tickCount = 0;
    if (tickCount % 10 == 0)
    {
      tickCount = 0;
      delayTime --;
    } 
    else
    {
      tickCount ++;
    }
    
    analogWrite(motorPin1, dutyCycle); //rotates motor
    digitalWrite(motorPin2, LOW);    // set the Pin motorPin2 LOW
    delay(delayTime);
    analogWrite(motorPin2, dutyCycle); //rotates motor
    digitalWrite(motorPin1, LOW);    // set the Pin motorPin2 LOW
    delay(delayTime);
  }
}

